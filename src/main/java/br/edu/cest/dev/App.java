package br.edu.cest.dev;


public class App 
{

	public static void main( String[] args ) {
        
        Book book01 = new Book();
        
        book01.setAuthor("Machado de Assis");
        book01.setEdition("2º");
        book01.setVolume("2");
        
		
		Item item01 = new Item();
		
		item01.setTitle("O Alienista");
		item01.setPublisher("Moderna");
		item01.setYearPublisher("1882");
		item01.setIsbn("9780850515060");
		item01.setPrice("R$ 8,50");
		
		
		
		book01.display();
		item01.display();
		
		
		// Classe ParImpar
		ParImpar num = new ParImpar();
		
		num.metodo();
			
		

    }

	
}
