package br.edu.cest.dev;

public class Item {
	private String title;
	private String publisher;
	private String yearPublisher;
	private String isbn;
	private String price;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public String getYearPublisher() {
		return yearPublisher;
	}
	public void setYearPublisher(String yearPublisher) {
		this.yearPublisher = yearPublisher;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	
public void display() {
	System.out.println( );
	System.out.println("=-=-=-=-=-=-=-=- Sobre o Item =-=-=-=-=-=-=-=-");
	System.out.println("Title: " + this.getTitle());
	System.out.println("Publisher: " + this.getPublisher());
	System.out.println("Year Publisher: " + this.getYearPublisher());
	System.out.println("ISBN: " + this.getIsbn());
	System.out.println("Price: " + this.getPrice());
	System.out.println("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-");
}
}
